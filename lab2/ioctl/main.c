#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h>

struct hd_geometry {
    unsigned char heads;
    unsigned char sectors;
    unsigned short cylinders;
    unsigned long start;
};

#define IOCTL_BASE	'W'
#define GET_HDDGEO	_IOR(IOCTL_BASE, 1, struct hd_geometry)

int main() {
    int fd = open("/dev/mydisk1", O_RDWR);
    if (fd == -1) {
        fputs("Can't open device\n", stderr);
        exit(EXIT_FAILURE);
    }

    struct hd_geometry geo;
    if (ioctl(fd, GET_HDDGEO, &geo)) {
        fprintf(stderr, "Can't perform HDDGEO. Error code: %lu\n", geo.start);
        exit(EXIT_FAILURE);
    }
    printf("Start: %lu\n", geo.start);
    printf("Heads: %d\n", geo.heads);
    printf("Cylinders: %d\n", geo.cylinders);
    printf("Sectors: %d\n", geo.sectors);
    return 0;
}

