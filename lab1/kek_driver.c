#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/uaccess.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/printk.h>

#include "device/device.h"
#include "device/init.h"

#define CLASS_NAME "kek"

MODULE_AUTHOR("Vitaliy Prikota, Katerina Mashina");

MODULE_LICENSE("GPL");

MODULE_DESCRIPTION("A linux driver for IO lab #1");

static struct file_operations device_fops = {
        .owner   = THIS_MODULE,
        .write   = dev_write,
        .read = dev_read,
        .open = dev_open,
        .release = dev_close
};

static struct proc_ops proc_fops = {
        .proc_read    = proc_read,
};

static int device_major_number = 0;
static int proc_entry = 0;
static struct device* c_dev;
static struct class* cl;
static const char device_name[] = "var3";
static struct list_head list;

int create(void) {
    device_major_number = register_device(device_name, &device_fops);
    if (!device_major_number) {
        printk(KERN_INFO "Failed to register device!\n");
        return -1;
    }

    printk(KERN_INFO "Major number is %d\n", device_major_number);

    cl = class_create(THIS_MODULE, CLASS_NAME);
    if (IS_ERR(cl)) {
        unregister_chrdev(device_major_number , device_name);
        printk(KERN_ALERT "%s: Failed to register device class\n", THIS_MODULE->name);
        return PTR_ERR(cl);
    }
    printk(KERN_INFO "%s: device class registered correctly\n", THIS_MODULE->name);

    c_dev = device_create(cl, NULL, MKDEV(device_major_number , 0), NULL, device_name);
    if (IS_ERR(c_dev)) {
        class_destroy(cl);
        unregister_chrdev(device_major_number , device_name);
        printk(KERN_ALERT "%s: Failed to create the device\n", THIS_MODULE->name);
        return PTR_ERR(c_dev);
    }
    printk(KERN_INFO "%s: device class created correctly\n", THIS_MODULE->name);
    printk(KERN_INFO "%s: /dev/%s is created\n", THIS_MODULE->name, device_name);

    proc_entry = proc_create(device_name, 0444, NULL, &proc_fops);
    printk(KERN_INFO "%s: /proc/%s is created\n", THIS_MODULE->name, device_name);

    INIT_LIST_HEAD(&list);
    set_list(&list);

    return 0;
}

static void destroy(void) {
    device_destroy(cl, MKDEV(device_major_number, 0));
    class_unregister(cl);
    class_destroy(cl);
    unregister_device(device_major_number, device_name);

    printk(KERN_INFO "%s: /dev/%s is destroyed\n", THIS_MODULE->name, device_name);

    proc_remove(proc_entry);
    printk(KERN_INFO "%s: /proc/%s is destroyed\n", THIS_MODULE->name, device_name);
    printk(KERN_INFO "Destroyed module successfully!\n");
}

module_init(create);
module_exit(destroy);