#include "device.h"
#include <linux/uaccess.h>
#include <linux/slab.h>

static int is_device_opened = 0;
static struct list_head* list = 0;

#define MAX_NUMBER_LENGTH 22

struct list_m_node {
    struct list_head list;
    long result;
};

int list_len(struct list_head* _list) {
    int length = 0;
    struct list_head* cur;
    list_for_each(cur, _list) {
        length += 1;
    }
    return length;
}

long count(const char __user* buffer, size_t len, loff_t* offset) {
    long result = 0;

    int num = 0;

    size_t i;
    size_t start = *offset;
    size_t stop = start + len;

    for (i = start; i < stop; i++) {
        char getChar;
        get_user(getChar, buffer + i);

        if (getChar >= '0' && getChar <= '9') {
            num = num * 10 + (getChar - '0');
        } else {
            result += num;
            num = 0;
        }
    }

    result += num;

    return result;
}


void set_list(struct list_head* _list) {
    list = _list;
}

ssize_t dev_write(struct file* filep, const char __user* buffer, size_t len, loff_t* offset) {
    printk(KERN_NOTICE "%s: write()\n", THIS_MODULE->name);
    struct list_m_node* node = kmalloc(sizeof(struct list_m_node), GFP_KERNEL);
    node->result = count(buffer, len, offset);
    list_add(&node->list, list);
    return *offset + len;
}

ssize_t dev_read(struct file* f, char __user* buf, size_t len, loff_t* off) {
    struct list_head* cur;
    struct list_m_node* element;
    size_t i = 0;

    list_for_each(cur, list) {
        element = list_entry(cur, struct list_m_node, list);
        printk(KERN_INFO "%s %ld: %ld\n", THIS_MODULE->name, i, element->result);
        i++;
    }
    return len;
}

int dev_open(struct inode* inode, struct file* file) {
    printk(KERN_NOTICE "%s: open()\n", THIS_MODULE->name);

    if (is_device_opened)
        return -EBUSY;

    is_device_opened = 1;

    try_module_get(THIS_MODULE);
    return 0;

}

int dev_close(struct inode* i, struct file* f) {
    printk(KERN_INFO "%s: close()\n", THIS_MODULE->name);
    is_device_opened = 0;

    module_put(THIS_MODULE);
    return 0;

}

ssize_t proc_read(struct file* file, char __user* ubuf, size_t count, loff_t* ppos) {
    char* buf = kzalloc(sizeof(char) * MAX_NUMBER_LENGTH * list_len(list), GFP_KERNEL);

    struct list_head* cur;
    struct list_m_node* element;
    size_t i = 0;

    list_for_each(cur, list) {
        element = list_entry(cur, struct list_m_node, list);
        snprintf(buf + (i * MAX_NUMBER_LENGTH), MAX_NUMBER_LENGTH, "%ld\n", element->result);
        i++;
    }

    size_t len = MAX_NUMBER_LENGTH * list_len(list);

    if (*ppos > 0 || count < len) {
        kfree(buf);
        return 0;
    }
    if (copy_to_user(ubuf, buf, len) != 0) {
        kfree(buf);
        return -EFAULT;
    }

    *ppos += len;

    kfree(buf);

    return len;
}