#pragma once
#include <linux/fs.h>

int register_device(char const * const device_name, struct file_operations const* const fops);
void unregister_device(const int device_major_number, char const * const device_name);