#pragma once
#include <linux/fs.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/module.h>
#include <linux/version.h>

ssize_t dev_write(struct file* filep, const char __user* buffer, size_t len, loff_t* offset);
int dev_open(struct inode* inode, struct file* file);
int dev_close(struct inode* i, struct file* f);
ssize_t dev_read(struct file* f, char __user* buf, size_t len, loff_t* off);
ssize_t proc_read(struct file* file, char __user *ubuf, size_t count, loff_t* ppos);
void set_list(struct list_head* _list);