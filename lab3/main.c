#include <linux/module.h>
#include <linux/version.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/moduleparam.h>
#include <linux/in.h>
#include <net/arp.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <linux/proc_fs.h>
#include <linux/tcp.h>
#define PORTS_LOG_SIZE 10

static const char* log_name = "C00L T4aFF1c Sn1FF34";
static struct proc_dir_entry* entry;
static char* parent_link = "lo";
module_param(parent_link, charp, 0);

static char* ifname = "vni%d";

static struct net_device_stats stats;

static struct net_device *child = NULL;
struct priv {
    struct net_device *parent;
};

struct packet_ports {
    int src_port;
    int dest_port;
    ktime_t timestamp;
};

static struct packet_ports ports_udp_log[PORTS_LOG_SIZE] = {0};
static struct packet_ports ports_tcp_log[PORTS_LOG_SIZE] = {0};
static int port_udp_log_pos = 0;
static int port_tcp_log_pos = 0;

static void log_ports(int src_port, int dest_port, ktime_t time, int protocol) {
    if (protocol == IPPROTO_UDP) {
        ports_udp_log[port_udp_log_pos].src_port = src_port;
        ports_udp_log[port_udp_log_pos].dest_port = dest_port;
        ports_udp_log[port_udp_log_pos].timestamp = time;
        port_udp_log_pos += 1;
        if (port_udp_log_pos >= PORTS_LOG_SIZE) {
            port_udp_log_pos = 0;
        }
    }
    else if(protocol == IPPROTO_TCP) {
        ports_tcp_log[port_tcp_log_pos].src_port = src_port;
        ports_tcp_log[port_tcp_log_pos].dest_port = dest_port;
        ports_tcp_log[port_tcp_log_pos].timestamp = time;
        port_tcp_log_pos += 1;
        if (port_tcp_log_pos >= PORTS_LOG_SIZE) {
            port_tcp_log_pos = 0;
        }
    }
}

static char check_frame(struct sk_buff *skb) {
    unsigned char *user_data_ptr = NULL;
    struct iphdr *ip = (struct iphdr *)skb_network_header(skb);
    struct udphdr *udp = NULL;
    struct tcphdr *tcp = NULL;

    printk(KERN_INFO "Got smthing proto: %d", ip->protocol);

    if (IPPROTO_UDP == ip->protocol) {
        udp = (struct udphdr*)((unsigned char*)ip + (ip->ihl * 4));
        printk(KERN_INFO "UDP: src(%d) dest(%d)", ntohs(udp->source), ntohs(udp->dest));
        log_ports(ntohs(udp->source), ntohs(udp->dest), ktime_get_real(), IPPROTO_UDP);
    }
    if (IPPROTO_TCP == ip->protocol) {
        tcp = (struct tcphdr*)((unsigned char*)ip + (ip->ihl) * 4);
        printk(KERN_INFO "TCP: src(%d) dest(%d)", ntohs(tcp->source), ntohs(tcp->dest));
        log_ports(ntohs(tcp->source), ntohs(tcp->dest), ktime_get_real(), IPPROTO_TCP);
    }
    return ip->protocol;
}

static rx_handler_result_t handle_frame(struct sk_buff **pskb) {
    if (check_frame(*pskb) == IPPROTO_UDP || check_frame(*pskb) == IPPROTO_TCP) {
        stats.rx_packets++;
        stats.rx_bytes += (*pskb)->len;
    }
    (*pskb)->dev = child;
    return RX_HANDLER_ANOTHER;
}

static int open(struct net_device *dev) {
    netif_start_queue(dev);
    printk(KERN_INFO "[%s]: device opened", dev->name);
    return 0;
}

static int stop(struct net_device *dev) {
    netif_stop_queue(dev);
    printk(KERN_INFO "[%s]: device closed", dev->name);
    return 0;
}

static netdev_tx_t start_xmit(struct sk_buff *skb, struct net_device *dev) {
    struct priv *priv = netdev_priv(dev);

    if (check_frame(skb) == IPPROTO_UDP || check_frame(skb) == IPPROTO_TCP) {
        stats.tx_packets++;
        stats.tx_bytes += skb->len;
    }

    if (priv->parent) {
        skb->dev = priv->parent;
        skb->priority = 1;
        dev_queue_xmit(skb);
        return 0;
    }
    return NETDEV_TX_OK;
}

static void write_to_output_buffer(const int stats_buffer_size, size_t *buffer_offset, int index, char* tmp_buffer,
                                   char* stats_buffer, struct packet_ports *ports_log) {
    if (stats_buffer_size - *buffer_offset > 0) {
        snprintf(tmp_buffer, 256, "%d\t\t%d\t\t%lld\n", ports_log[index].src_port, ports_log[index].dest_port,
                 ports_log[index].timestamp);
        snprintf(stats_buffer + *buffer_offset, stats_buffer_size - *buffer_offset, "%s", tmp_buffer);
        *buffer_offset += strlen(tmp_buffer);
    }
}

static ssize_t proc_read(struct file *file, char __user * buf, size_t len, loff_t* off) {
    ssize_t res;
    const int stats_buffer_size = 256 + 200 * PORTS_LOG_SIZE;
    size_t buffer_offset;
    int i;
    char *stats_buffer = kmalloc(stats_buffer_size, GFP_KERNEL);

    snprintf(stats_buffer, stats_buffer_size,
             "Packets: %lu.\nTotal bytes: %lu\n\n------------UDP------------\nsrc\t\tdest\t\ttimestamp\n",
             stats.rx_packets, stats.rx_bytes);
    buffer_offset = strlen(stats_buffer);

    for (i = 0; i < PORTS_LOG_SIZE; i++) {
        static char tmp_buffer[256];
        write_to_output_buffer(stats_buffer_size, &buffer_offset, i, tmp_buffer, stats_buffer, ports_udp_log);
    }

    snprintf(stats_buffer + buffer_offset, stats_buffer_size - buffer_offset,
             "------------TCP------------\nsrc\t\tdest\t\ttimestamp\n");
    buffer_offset += strlen("------------TCP------------\nsrc\t\tdest\t\ttimestamp\n");

    for (i = 0; i < PORTS_LOG_SIZE; i++) {
        static char tmp_buffer[256];
        write_to_output_buffer(stats_buffer_size, &buffer_offset, i, tmp_buffer, stats_buffer, ports_tcp_log);
    }

    if ((len < buffer_offset) || (*off > 0)) {
        res = 0;
        goto ret;
    }
    if (copy_to_user(buf, stats_buffer, buffer_offset) != 0) {
        res = -EFAULT;
        goto ret;
    }

    *off += buffer_offset;
    res = buffer_offset;
    ret:
    kfree(stats_buffer);
    return res;
}

static struct file_operations proc_fops = {
        .owner = THIS_MODULE,
        .read = proc_read,
};

static struct net_device_stats *get_stats(struct net_device *dev) {
    return &stats;
}

static struct net_device_ops crypto_net_device_ops = {
        .ndo_open = open,
        .ndo_stop = stop,
        .ndo_get_stats = get_stats,
        .ndo_start_xmit = start_xmit
};

static void setup(struct net_device *dev) {
    int i;
    ether_setup(dev);
    memset(netdev_priv(dev), 0, sizeof(struct priv));
    dev->netdev_ops = &crypto_net_device_ops;

    //fill in the MAC address with a phoney
    for (i = 0; i < ETH_ALEN; i++)
        dev->dev_addr[i] = (char)i;
}

int __init vni_init(void) {
    int err = 0;
    struct priv *priv;

    entry = proc_create("var3", 0777, NULL, &proc_fops);
    printk(KERN_INFO "[%s]: created proc file\n", log_name);

    child = alloc_netdev(sizeof(struct priv), ifname, NET_NAME_UNKNOWN, setup);
    if (child == NULL) {
        printk(KERN_ERR "[%s]: allocate error", log_name);
        return -ENOMEM;
    }
    priv = netdev_priv(child);
    priv->parent = __dev_get_by_name(&init_net, parent_link); //parent interface
    if (!priv->parent) {
        printk(KERN_ERR "[%s]: no such device: %s", log_name, parent_link);
        free_netdev(child);
        return -ENODEV;
    }
    if (priv->parent->type != ARPHRD_ETHER && priv->parent->type != ARPHRD_LOOPBACK) {
        printk(KERN_ERR "[%s]: illegal net type", log_name);
        free_netdev(child);
        return -EINVAL;
    }

    //copy IP, MAC and other information
    memcpy(child->dev_addr, priv->parent->dev_addr, ETH_ALEN);
    memcpy(child->broadcast, priv->parent->broadcast, ETH_ALEN);
    if ((err = dev_alloc_name(child, child->name))) {
        printk(KERN_ERR "[%s]: allocate name, error %i", log_name, err);
        free_netdev(child);
        return -EIO;
    }

    register_netdev(child);
    rtnl_lock();
    netdev_rx_handler_register(priv->parent, &handle_frame, NULL);
    rtnl_unlock();
    printk(KERN_INFO "[%s]: module loaded", log_name);
    printk(KERN_INFO "[%s]: created device %s", log_name, child->name);
    printk(KERN_INFO "[%s]: registered rx handler for %s", log_name, priv->parent->name);
    return 0;
}

void __exit vni_exit(void) {
    struct priv *priv = netdev_priv(child);

    proc_remove(entry);
    printk(KERN_INFO "[%s]: proc file is deleted\n", log_name);

    if (priv->parent) {
        rtnl_lock();
        netdev_rx_handler_unregister(priv->parent);
        rtnl_unlock();
        printk(KERN_INFO "[%s]: unregister rx handler for %s", log_name, priv->parent->name);
    }
    unregister_netdev(child);
    free_netdev(child);
    printk(KERN_INFO "[%s]: module is unloaded", log_name);
}

module_init(vni_init);
module_exit(vni_exit);

MODULE_AUTHOR("Vitaliy and Ekaterina");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("IO lab3 for network traffic sniffing");